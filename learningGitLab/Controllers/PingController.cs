using Microsoft.AspNetCore.Mvc;

namespace learningGitLab.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PingController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;

        public PingController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get(string ?input)
        {
            if (string.IsNullOrEmpty(input))
                input = string.Empty;

            return string.Format("ping v2 {0}", input.ToLower());
        }


        public string GetNha(string? input)
        {
            if (string.IsNullOrEmpty(input))
                input = string.Empty;

            return string.Format("ping v2 {0}", input.ToLower());
        }

        public string GetNha2(string? input)
        {
            if (string.IsNullOrEmpty(input))
                input = string.Empty;

            return string.Format("ping v2 {0}", input.ToLower());
        }
    }
}